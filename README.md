Feedback 10/2 - Mira

Mira's program is an ice-cream, with three scoops. I assume one with chocolate, one with strawberry and one with pistachio. 
So even without many details it is easy for us to see, which kind of ice-cream Mira have chosen, with the use of a color, 
which is very similar to the color of the actual ice-cream. The same applies, with the use and shape of the elements. 
Mira have used the ellipses for the scoops and the 'nuts' in the pistachio ice-cream. From my point of view, 
it would have been more obvious to use the circles.

Reflection.
It is an instructive experience to read Mira's work, both for further learning and inspiration for my next miniex. 
We share a common goal and helping each other to reach it. Which is why sharing our miniexs and giving feedback to each other 
is contributing to an improved understanding of literacy. 
	
Reading Mira's code puts my own I perspective, improves my literacy 
and gives me an illustrative understanding of code in general.